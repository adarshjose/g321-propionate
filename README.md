Goal: Determine if there is a conserved propionate response to glycans across multiple communities. 

Experiment Design:
	Communities: 6 communities from healthy donors. 
5 Screening slurries (FS1001, FS1002, FS123-8, FS877-3, FS9-10). 
1 additional slurry saved from fresh vs frozen ( B.2). 

	Time points: 23 hr, 39 hr and 45 hr.
	Media: CM.3

Key Takeaways: 
* Most glycans shows significant (>2 fold change vs control) propionate response across communities by 45 hours
* FS9-10 produces more propionate across glycans than other communities 
* The kinetics of propionate production when treated with a compound varies across community
* Several glycans are making propionate at the cost of butyrate
* Propionate/Butyrate levels increases with decrease in acetate levels

Recommendations: 
	16s sequencing recommended for the samples to identify taxonomic association with propionate production.

